﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Final : MonoBehaviour
{
    public GameObject modifiedEnergy;
    public GameObject player;
    public static bool started = false;

    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            //TextChanger.textShow = true;
            //TextChanger.textState = 31;
            started = false;
            StartCoroutine(BigBang());
        }
    }

    IEnumerator BigBang()
    {
       // player.GetComponent<Behaviour>().enabled = false;
        yield return new WaitForSeconds(0.2f);
        int count = 0;
        while (count < 100)
        {
            count++;
            GameObject obj = Instantiate(modifiedEnergy, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.05f);
        }
        yield return new WaitForSeconds(3f);
        StartCoroutine(Small());
    }

    IEnumerator Small()
    {
        Transform playerTransform = player.transform;
        float coef = 0.6f;
        while (playerTransform.localScale.x > 0)
        {
            playerTransform.localScale = new Vector3(playerTransform.localScale.x - coef, playerTransform.localScale.x - coef,
                           playerTransform.localScale.x - coef);
            yield return new WaitForSeconds(0.05f);
        }
    }
}

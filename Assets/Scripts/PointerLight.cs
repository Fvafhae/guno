﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerLight : MonoBehaviour
{
    public Collider2D player;
    public GameObject lighter_1;
    public GameObject lighter_2;
    private bool was = false;
    private bool touched = false;

    private void Update()
    {
        try
        {
            if (gameObject.GetComponent<CircleCollider2D>().IsTouching(player))
            {
                if (!TextChanger.textShow && !was)
                {
                    TextChanger.textShow = true;
                    TextChanger.textState = 9;
                    touched = true;
                }                

                if (!was && (Input.GetKey(KeyCode.E)))// || Input.GetKey(KeyCode.Space)))
                {
                    if (PlayerTriggers.energyCount >= 2)
                    {
                        was = true;
                        PlayerTriggers.energyCount -= 2;                        
                        PlayerTriggers.lightedPointers += 1;
                        lighter_1.GetComponent<Animator>().enabled = true;//new Color32(255, 255, 255, 255);
                        lighter_2.GetComponent<Animator>().enabled = true;//new Color32(255, 255, 255, 255);
                    }
                }
            }
            else if (TextChanger.textShow && TextChanger.textState == 9 && touched)
            {
                TextChanger.textShow = false;
                touched = false;
            }
        }
        catch { }
    }    
}

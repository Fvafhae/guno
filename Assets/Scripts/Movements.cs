﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movements : MonoBehaviour
{
    public List<GameObject> levels;
    public GameObject trail;
    public Camera cam;

    private Transform t;

    public static bool movementAllowed = true;

    private float speed = 10f;
    private bool was = false;

    public static int level = 1;
    public int lastLevel;
    public bool mov;
    //private bool finish = false;

    void Start()
    {
        t = transform;
        lastLevel = level;
    }

    void Update()
    {
        if (lastLevel != level)// && mov)
        {
            lastLevel = level;
            //level = lastLevel;
            LevelSwitch();
        }

        if (movementAllowed)
        {
            trail.SetActive(true);
            Vector2 pos = t.position;

            if (Input.GetKey(KeyCode.W))
            {
                pos += new Vector2(0f, 1f) * speed * Time.deltaTime;
                NextDialog();
            }

            if (Input.GetKey(KeyCode.S))
            {
                pos += new Vector2(0f, -1f) * speed * Time.deltaTime;
                NextDialog();
            }

            if (Input.GetKey(KeyCode.A))
            {
                pos += new Vector2(-1f, 0f) * speed * Time.deltaTime;
                NextDialog();
            }

            if (Input.GetKey(KeyCode.D))
            {
                pos += new Vector2(1f, 0f) * speed * Time.deltaTime;
                NextDialog();
            }

            t.position = pos;
        }
        else trail.SetActive(false);
    }

    void LevelSwitch()
    {
        StartCoroutine(DelaySwitch());        
    }

    void NextDialog()
    {
        if (level == 1 && !was)
        {
            was = true;
            StartCoroutine(Delay());
        }
    }

    IEnumerator DelaySwitch()
    {
        movementAllowed = false;

        yield return new WaitForSeconds(1.1f);
        t.position = Vector3.zero;
        t.localScale = new Vector3(1f, 1f, 1f);
        cam.orthographicSize = 5.1f;
        gameObject.GetComponent<Animator>().Play("small", -1, 0f);
        gameObject.GetComponent<Animator>().enabled = false;
        yield return new WaitForSeconds(0.2f);
        GameObject comparison = levels[level - 1];
        foreach (GameObject lvl in levels)
        {
            if (lvl == comparison)
            {
                lvl.SetActive(true);
            }
            else lvl.SetActive(false);
        }
        switch (level)
        {
            case 2:
                TextChanger.textState = 5;
                break;
            case 3:
                TextChanger.textState = 8;
                break;
            case 4:
                TextChanger.textState = 10;
                break;
            case 5:
                TextChanger.textState = 12;
                break;
            case 6:
                TextChanger.textState = 13;
                break;
            case 7:
                TextChanger.textState = 14;
                break;
            case 8:
                TextChanger.textState = 17;
                break;
            case 9:
                TextChanger.textState = 18;
                break;
            case 10:
                TextChanger.textState = 19;
                break;
            case 11:
                TextChanger.textState = 21;
                break;
            case 12:
                TextChanger.textState = 22;
                break;
            case 13:
                TextChanger.textState = 23;
                break;
            case 16:
                TextChanger.textState = 24;
                break;
            case 17:
                TextChanger.textState = 25;
                break;
            case 20:
                TextChanger.textState = 26;
                break;
            case 22:
                TextChanger.textState = 27;
                break;
            case 23:
                TextChanger.textState = 28;
                break;
            case 24:
                TextChanger.textState = 29;
                break;
            default:
                TextChanger.textState = -4;
                movementAllowed = true;
                break;
        }
        TextChanger.textShow = true;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        //yield return new WaitForSeconds(1f); //1f
        movementAllowed = false;
        yield return new WaitForSeconds(0.1f);
        t.position = Vector3.zero;
        TextChanger.textState++;
    }
}

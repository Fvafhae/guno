﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowing : MonoBehaviour
{
    private bool was = false;
    private Vector3 startPosition;
    private readonly float coefWidth = 10f;

    private void Start()
    {
        startPosition = gameObject.transform.position;
    }

    private void Update()
    {
        if (!was)
        {
            Collider2D col = Physics2D.OverlapArea(new Vector2(transform.position.x + coefWidth, 
                transform.position.y + coefWidth*0.55f), new Vector2(transform.position.x - coefWidth, 
                transform.position.y - coefWidth*0.55f), 1);

            //Debug.Log(col);

            if (col != null && col.tag == "Player")
            {
                was = true;
                StartCoroutine(Capturer(col.transform));
            }
        }
    }

    IEnumerator Capturer(Transform player)
    {
        float speedCoef = 5f;
        while (!PlayerTriggers.isReturned && !PortalEntering.transition)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.position, speedCoef * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
        yield return new WaitForSeconds(0.1f);

        was = false;
        gameObject.transform.position = startPosition;
    }

}

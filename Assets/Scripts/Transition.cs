﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    public GameObject block;
    public GameObject text;

    public void Transit()
    {
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        block.GetComponent<Animator>().enabled = true;
        gameObject.GetComponent<Animator>().enabled = true;
        text.SetActive(false);
        yield return new WaitForSeconds(0.6f);
        SceneManager.LoadScene("Game");
    }
}

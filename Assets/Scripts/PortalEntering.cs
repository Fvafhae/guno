﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalEntering : MonoBehaviour
{
    public static int neededP;
    public static bool transition = false;
    public int neededEnergy;
    public int neededPointers;

    private void Update()
    {
        neededP = neededPointers;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameObject player = other.gameObject;
            if (PlayerTriggers.energyCount >= neededEnergy && PlayerTriggers.lightedPointers >= neededPointers)
            {
                PlayerTriggers.energyCount = 0;
                PlayerTriggers.lightedPointers = 0;
                if (PlayerTriggers.collectingPhase)
                {
                    PlayerTriggers.deliveredEnergy += 3;
                }
                transition = true;
                StartCoroutine(NextLevelTransition(player));
            }
        }
    }

    IEnumerator NextLevelTransition(GameObject player)
    {
        player.GetComponent<Animator>().enabled = true;
        player.transform.position = Vector2.MoveTowards(player.transform.position, transform.position, 30f * Time.deltaTime);
        Movements.level++;
        transition = false;
        yield return new WaitForSeconds(1.5f);        
    }
}

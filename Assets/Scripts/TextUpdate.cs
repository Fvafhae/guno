﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextUpdate : MonoBehaviour
{
    private TextMeshProUGUI text;

    void Start()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name == "Energy")
        {
            text.text = PlayerTriggers.energyCount.ToString();
        }
        else if (gameObject.name == "Pointers")
        {
            text.text = PlayerTriggers.lightedPointers + " / " + PortalEntering.neededP;
        }
        else if (gameObject.name == "Delivered")
        {
            text.text = PlayerTriggers.deliveredEnergy + " / 40";
        }
    }
}

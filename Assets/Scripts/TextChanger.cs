﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextChanger : MonoBehaviour
{
    private TextMeshProUGUI text;

    public GameObject face;
    public GameObject lvl1;
    public GameObject energyText;
    public GameObject pointersText;
    public GameObject deliveredEnergyText;

    public static int textState = 0;
    public static bool textShow = true;
    private bool waiting = false;
    private bool wasPressed = false;

    void Start()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
    }

    //Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !wasPressed && textState != 0)
        {
            wasPressed = true;
            List<int> next = new List<int>() { -4,-3,-2,-1, 4, 7, 9, 11, 12, 13, 16, 17, 18, 20, 21, 22, 23, 24, 25,
               26, 27, 28}; 
            if (next.Contains(textState))
            {
                textShow = false;
                StopAllCoroutines();
                waiting = false;
                Movements.movementAllowed = true;
            }
            else if (textState < 29)
            {
                textState++;
                StopAllCoroutines();
                waiting = false;
            }

            if (Movements.level == 1 && textState == 4)
            {
                lvl1.SetActive(true);
                energyText.SetActive(true);
            }
            wasPressed = false;
        }
        
        //Debug.Log(textState);
        if (textShow)
        {
            if (!waiting)
            {
                if (textState != 0)
                {
                    face.SetActive(true);
                }

                switch (textState)
                {
                    case -4:
                        text.text = "";
                        textShow = false;
                        break;
                    case -3:
                        text.text = "It seems that your energy was taken. You need to do everything again.";
                        break;
                    case -2:
                        text.text = "Have I told you to touch them?!";
                        break;
                    case -1:
                        text.text = "Don't try to run away from me!";
                        break;
                    case 0:
                        text.text = "Press to move:      W  \n                       A S D";
                        break;
                    //Lavel 1
                    case 1:
                        text.text = "Oh, an intelligent Gu energy. My experiments weren't for nothing.";
                        StartCoroutine(DelayNext());
                        break;
                    case 2:
                        text.text = "Hi, I'm NOTHING.\nYou've just been created.\nI will call you GUNO.";
                        StartCoroutine(DelayNext());
                        break;
                    case 3:
                        text.text = "Your only purpose for now is to collect energy balls for me.";
                        StartCoroutine(DelayNext());
                        break;
                    case 4:
                        text.text = "When you will be done, enter in that portal.";
                        StartCoroutine(DelayStop());                    
                        break;
                    //Lavel 2
                    case 5:
                        text.text = "Everytime I will be moving you to the places with energy.";
                        StartCoroutine(DelayNext());
                        break;
                    case 6:
                        text.text = "If you do not collect enough energy, you will stay here forever.";
                        StartCoroutine(DelayNext());
                        break;
                    case 7:
                        text.text = "And of course everytime you will start with no energy.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 3
                    case 8:
                        pointersText.SetActive(true);
                        text.text = "It was easy. Now portals will not work until you light up all pointers.";
                        StartCoroutine(DelayNext());
                        break;
                    case 9:
                        face.SetActive(false);
                        text.text = "Press 'E' to light up pointers with collected energy";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 4
                    case 10:
                        text.text = "Once I have tryed to make energy collectors. Well, it was a failure.";
                        StartCoroutine(DelayNext());
                        break;
                    case 11:
                        text.text = "They have formed big clusters. Don't touch them or your energy will be gone.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 5
                    case 12:
                        text.text = "You are learning fast.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 6
                    case 13:
                        text.text = "Keep collecting energy balls for me.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 7
                    case 14:
                        text.text = "Once I have accidently created aggressive energy. I call them AGERS.";
                        StartCoroutine(DelayNext());
                        break;
                    case 15:
                        text.text = "Once they see you, they will be following you.";
                        StartCoroutine(DelayNext());
                        break;
                    case 16:
                        text.text = "I hope you understand that they can take all your energy.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 8
                    case 17:
                        text.text = "I like your speed. I myself am not able to collect energy so fast.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 9
                    case 18:
                        text.text = "I like your speed. I myself am not able to collect energy so fast.";
                        StartCoroutine(DelayStop());
                        break;
                    //Lavel 10
                    case 19:
                        deliveredEnergyText.SetActive(true);
                        PlayerTriggers.collectingPhase = true;
                        text.text = "If you deliver me through portals 40 more energy balls,";
                        StartCoroutine(DelayNext());
                        break;
                    case 20:
                        text.text = "we can try something intersting.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 11
                    case 21:
                        text.text = "I am busy preparing my new experiment.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 12
                    case 22:
                        text.text = "Can you deliver me so many energy balls?";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 13
                    case 23:
                        text.text = "I won't be able to communicate for some time. Good luck.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 16
                    case 24:
                        text.text = "Hi, I am back.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 17
                    case 25:
                        text.text = "You've collected more than half of needed energy balls.";
                        StartCoroutine(DelayStop());
                        break;                    
                    //Level 20
                    case 26:
                        text.text = "Only 10 energy balls are left to deliver.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 22
                    case 27:
                        text.text = "4 more energy balls and we can start my experiment.";
                        StartCoroutine(DelayStop());
                        break;
                    //Level 23
                    case 28:
                        text.text = "I found a place with some free energy balls.";
                        StartCoroutine(DelayStop());
                        break;
                    //Final (24 level)
                    case 29:
                        text.text = "Finally. Let's begin my experiment.";
                        StartCoroutine(DelayNext(3f));
                        break;
                    case 30:
                        text.text = "You stay still, I will be throuwing modified energy balls in you.";
                        StartCoroutine(DelayNext());
                        break;
                    case 31:
                        Final.started = true;
                        face.SetActive(false);
                        text.text = "";
                        StartCoroutine(DelayNext(3f));
                        break;
                    case 32:
                        text.color = new Color32(0, 0, 0, 255);
                        face.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
                        face.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, 255);
                        face.SetActive(true);
                        text.text = "Oh no, something went wrong. Why so many energy balls were thrown?!";
                        StartCoroutine(DelayNext(7.3f));
                        break;
                    case 33:
                        text.color = new Color32(255, 255, 255, 255);
                        face.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        face.transform.GetChild(0).gameObject.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        text.text = "Once again failure.\nGUNO has exploded.";
                        StartCoroutine(DelayNext());
                        break;
                    case 34:
                        text.text = "He will create a new universe and will have an interesting eternity.";
                        StartCoroutine(DelayNext());
                        break;
                    case 35:
                        text.text = "I am left with nothing. Should I start my experiments again?";
                        StartCoroutine(DelayEnd());
                        break;
                }
            }
        }
        else
        {
            text.text = "";
            face.SetActive(false);
            StopAllCoroutines();
            waiting = false;
            PlayerTriggers.isChanging = false;
        }
    }

    IEnumerator DelayEnd(float time = 4f)
    {
        waiting = true;
        yield return new WaitForSeconds(time);//(3f);
        waiting = false;
        SceneManager.LoadScene("Main");
    }

    IEnumerator DelayNext(float time = 4f)
    {
        waiting = true;
        yield return new WaitForSeconds(time);//(3f);
        waiting = false;
        textState++;
    }

    IEnumerator DelayStop(float time = 4f)
    {
        waiting = true;
        yield return new WaitForSeconds(time);
        textShow = false;        
        if (Movements.level == 1)
        {
            lvl1.SetActive(true);
            energyText.SetActive(true);
        }
        waiting = false;
        Movements.movementAllowed = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTriggers : MonoBehaviour
{
    CircleCollider2D player;
    List<Collider2D> materials = new List<Collider2D>();
    ContactFilter2D filter;

    private Transform playerTransform;

    public static int energyCount = 0;
    public static int lightedPointers = 0;
    public static int deliveredEnergy = 0;

    public static bool isReturned = false;
    public static bool collectingPhase = false;

    public static bool isChanging = false;

    private void Start()
    {
        player = gameObject.GetComponent<CircleCollider2D>();
        filter.layerMask = 1;
        playerTransform = player.transform;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Material")
        {
            other.gameObject.SetActive(false);
            energyCount += 1;
            //Debug.Log(energyCount);
        }
        else if (other.gameObject.tag == "ModifiedEnergy" && !isChanging)
        {
            float coef = 0.3f;
            playerTransform.localScale = new Vector3(playerTransform.localScale.x + coef, playerTransform.localScale.x + coef,
                playerTransform.localScale.x + coef);
            Destroy(other.gameObject.transform.parent.gameObject);
        }
        else if (other.gameObject.tag == "Border" && !isChanging)
        {
            isChanging = true;
            Movements.movementAllowed = false;
            StartCoroutine(MovePlayer(-1));
            isReturned = true;
        }
        else if (other.gameObject.tag == "Glue" && !isChanging)
        {
            isChanging = true;
            Movements.movementAllowed = false;
            StartCoroutine(MovePlayer(-2));
            isReturned = true;
        }
        else if (other.gameObject.tag == "Enemy" && !isChanging)
        {
            isChanging = true;
            Movements.movementAllowed = false;
            StartCoroutine(MovePlayer(-3));
            isReturned = true;
        }
    }

    IEnumerator MovePlayer(int message)
    {
        int previousState = TextChanger.textState;
        transform.position = Vector3.zero;
        TextChanger.textState = message;
        TextChanger.textShow = true;     
        
        yield return new WaitForSeconds(0.5f);
        Movements.movementAllowed = true;
        yield return new WaitForSeconds(0.5f);
        isReturned = false;
        isChanging = false;

        yield return new WaitForSeconds(1f);
        TextChanger.textShow = false;
    }
}